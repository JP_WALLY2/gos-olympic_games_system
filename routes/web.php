<?php

//PAGINA INICIAL
Route::get('/', 'IndexController@index')->name('/');

// Route::get('/testeMiddleware', function () {
//     return "Você está logado";
// })->middleware('check');

//ROTAS DE AUTENTICAÇÃO
Route::post('newUser', 'Auth\RegisterController@store')->name('newUser');
Route::post('loginUser', 'Auth\LoginController@logar')->name('loginUser');
Route::post('logoutUser', 'Auth\LoginController@sair')->name('logoutUser');

Route::group(['prefix' => 'admin', 'middleware' => 'check'], function(){

//confrontos
Route::resource('/index', 'Admin\IndexController');
Route::get('/index/destacar/{id}', 'Admin\IndexController@destacar')->name('index.destacar');
Route::post('/index/str', 'Admin\IndexController@str')->name('index.str');

//bar
Route::post('/index/cadBar', 'Admin\IndexController@cadBar')->name('index.cadBar');
Route::delete('/index/excluirBar/{id}', 'Admin\IndexController@excluirBar')->name('index.excluirBar');

//programação
Route::post('/index/cadProg', 'Admin\IndexController@cadProg')->name('index.cadProg');
Route::delete('/index/excluirProg/{id}', 'Admin\IndexController@excluirProg')->name('index.excluirProg');



});

Route::group(['middleware' => 'check'], function(){
    Route::resource('/index', 'IndexController');
});

