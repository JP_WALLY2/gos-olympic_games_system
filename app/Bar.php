<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    public $timestamps = false;

    protected $fillable = ['nome', 'preco', 'users_id'];

    // retira a máscara com "." e "," antes da inserção 
    public function setPrecoAttribute($value) {
        $novo1 = str_replace('.', '', $value);    // retira o ponto
        $novo2 = str_replace(',', '.', $novo1);   // substitui a , por .
        $this->attributes['preco'] = $novo2;
    }

    public function users() {
        return $this->belongsTo('App\User');
    }
}
