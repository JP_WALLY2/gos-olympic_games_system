<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confronto extends Model
{
    public $timestamps = false;

    protected $fillable = ['times_id', 'placar1', 'subPlacar1', 'times_id2', 'placar2', 'subPlacar2', 'users_id', 'jogos_id', 'status', 'fase', 'destaque', 'vencedor'];

    public function times() {
        return $this->belongsTo('App\Time', 'times_id', 'id');
    }

    public function times2() {
        return $this->belongsTo('App\Time', 'times_id2', 'id');
    }

    public function users() {
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function jogos() {
        return $this->belongsTo('App\Jogo', 'jogos_id', 'id');
    }
}
