<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programacao extends Model
{
    public $timestamps = false;

    protected $fillable = ['texto', 'hora', 'users_id'];

    public function users() {
        return $this->belongsTo('App\User');
    }
}
