<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jogo extends Model
{
    protected $fillable = ['nome', 'categoria', 'pontuacao'];

    public function confrontos() {
 	    return $this->hasMany('App\Confronto', 'confronto_id', 'id');
    }

    public function campeaos() {
        return $this->hasMany('App\Campeao', 'campeao_id', 'id');
    }

    // public function getCategoriaAttribute($value) {
    //     if ($value == "Masc") {
    //         return "Masculino";
    //     } else if ($value == "Fem") {
    //         return "Feminino";
    //     }
    // }

}
