<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FormProdutosRequest;
use App\Jogo;
use App\Time;
use App\User;
use App\Confronto;
use App\Campeao;
use App\Bar;
use App\Programacao;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //variavel co os jogos
        $jogos = Jogo::whereNotIn('jogos.id', function($q) {
            $q->join('confrontos', 'jogos.id', 'confrontos.jogos_id')
            ->select('jogos.id')
            ->from('jogos');
        })->orderBy('nome')->get();
        //variavel com os times por nome
        $times = Time::orderBy('nome')->get();
        //variavel com todos os confrontos
        $confrontos = Confronto::all();
        //variavel com todos os campeões
        $campeaos = Campeao::all();
        //variavel com todos os produtos do bars
        $bars = Bar::orderBy('preco', 'DESC')->paginate(3);
        //variavel com todo A PROGRAMAÇÃO
        $programacao = Programacao::orderBy('hora', 'ASC')->paginate(3);

        //retorna a view
        return view('Admin/index',[
            //passa os jogos, times e confrontos
            'jogos'=>$jogos,
            'times'=>$times,
            'confrontos' =>$confrontos,
            'campeaos'=>$campeaos,
            'bars'=>$bars,
            'programacao'=>$programacao
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function str(Request $request)
    {
        //cadastro na tabela confronto

        //primeiro confronto
        $conf1['jogos_id'] = $request->get('jogos_id');
        $conf1['times_id'] = $request->get('time1_id');
        $conf1['times_id2'] = $request->get('time2_id');
        $conf1['fase'] = 'Mata-Mata';
        $conf1['users_id'] =  auth()->user()->id;
        
        //segundo confronto
        $conf2['jogos_id'] = $request->get('jogos_id');
        $conf2['times_id'] = $request->get('time3_id');
        $conf2['times_id2'] = $request->get('time4_id');
        $conf2['fase'] = 'Mata-Mata';
        $conf2['users_id'] =  auth()->user()->id;

        //terceiro confronto
        $conf3['jogos_id'] = $request->get('jogos_id');
        $conf3['times_id'] = $request->get('time5_id');
        $conf3['times_id2'] = $request->get('time6_id');
        $conf3['fase'] = 'Semi-Final';
        $conf3['users_id'] =  auth()->user()->id;

        //quarto confronto
        $conf4['jogos_id'] = $request->get('jogos_id');
        $conf4['times_id'] = $request->get('time7_id');
        $conf4['times_id2'] = $request->get('time8_id');
        $conf4['fase'] = 'Final';
        $conf4['users_id'] =  auth()->user()->id;

        //variavel com todos as entradas de times
        $timesReq = $request->get('time1_id') && $request->get('time2_id') && $request->get('time3_id') && 
        $request->get('time4_id') && $request->get('time5_id');

        //varial da entrado do jogo
        $jogoReq =  $request->get('jogos_id');

        //se a entrada dos times e do jogo for maior que 0
        //e se a entrada dos times e do jogo for diferente de nulo (vazio)
        if($timesReq > 0 && $jogoReq > 0 || $timesReq != null && $jogoReq != null){
            
            //criar os confrontos
            $c1 = Confronto::create($conf1);
            $c2 = Confronto::create($conf2);
            $c3 = Confronto::create($conf3);
            $c4 = Confronto::create($conf4);
            
            //redireciona para o admin/inde com a mensagem de cadastro feito
            return redirect('admin/index')->with('status', 'Cadastro feito com sucesso!');

        //se a entrada dos times for = a 0 e a do jogo for maior que 0
        //se a entrada dos times for = a nulo (vazio) e a do jogo for diferente que nulo (vazio)
        }else if($timesReq == 0 && $jogoReq > 0 || $timesReq == null && $jogoReq != null){
              //redireciona para o admin/inde com a mensagem de erro ao cadastrar e time não selecionado
            return redirect('admin/index')->with('StatusErr', 'Erro ao fazer o cadastro')->with('err', 'Time não selecionado');

        //se a entrada dos times for maior que 0 e a do jogo for = 0
        //se a entrada dos times não for nulo (vazio) e a do jogo for = a nulo (vazio)
        }else if($timesReq > 0 && $jogoReq == 0 || $timesReq != null && $jogoReq == null){
              //redireciona para o admin/inde com a mensagem de erro ao cadastrar e jogo não selecionado
            return redirect('admin/index')->with('StatusErr', 'Erro ao fazer o cadastro')->with('errj', 'Jogo não selecionado');

        }else{
              //redireciona para o admin/inde com a mensagem de erro ao cadastrar, time e jogo não selecionado
            return redirect('admin/index')->with('StatusErr', 'Erro ao fazer o cadastro')
            ->with('err', 'Time não selecionado')->with('errj', 'Jogo não selecionado');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //variavel que pega o placar e o subPlacar do time 1
         $placar1 = request()->input('placar1');
         $subPlacar1 = request()->input('subPlacar1');
        //variavel que pega o placar e o subPlacar do time 2
         $placar2 = request()->input('placar2');
         $subPlacar2 = request()->input('subPlacar2');
         //pega o id do time 1
         $TimeID1 = request()->input('times_id');
         //pega o id do time 2
         $TimeID2 = request()->input('times_id2');

        //variavel que pega o status do input
         $status = request()->input('status');

        //pega o id do jogo
        $jid = $request->input('jogos_id');
        //pega o confrontos com o id do jogo
        $c = Confronto::where('jogos_id', $jid)->get();
        //pega o id do confronto na segunda posição
        $idp2 = $c[2]->id;
        //pega o id do confronto na terceira posição
        $idp3 = $c[3]->id;

        // variaveis que armazenam a soma dos placar de cada time
        $somaTime1 = $placar1 + $subPlacar1;
        $somaTime2 = $placar2 + $subPlacar2;

        //se o status for igual a encerrado mas a soma do placar dos dois times for 0
        if($status == 'encerrado' && $somaTime1 == null && $somaTime2 == null ){
            return redirect('admin/index')
            ->with('StatusErr', 'Falha ao alterar o confronto');
            
        //se o status for igual a encerrado
        }elseif($status == 'encerrado'){

            //variavel que armazena o resto da divisão do id do confronto por 4 (define a fase do confronto)
            $resto = $id % 4;

            //se a soma do time 1 for maior que do time 2
            if($somaTime1 > $somaTime2){        
                //coloca o id na coluna vencedor
                $venc['vencedor'] = $TimeID1;  
                //variavel que pega o confronto do id informado
                $reg = Confronto::find($id);
                //variavel que atualiza o registro ($reg) com a variavel que armazena o id na coluna vencedor
                $v = $reg->update($venc);

                $ultimoVencedor = $c[3]->times_id;

                if($v){
                    if($resto == 1){
                        // variavel que pega o confronto do id informado
                        $reg = Confronto::find($idp2);
                         // coloca o id do time 1 no terceiro confronto (posição 2) no lugar do times_id2
                        $time2['times_id2'] = $TimeID1;
                        //variavel que atualiza o registro ($reg) com a variavel que armazena o id na coluna vencedor
                        $reg->update($time2);   
                    } else
                    if($resto == 2){
                            //variavel que pega o confronto do id informado
                            $r2 = Confronto::find($idp3);
                            // coloca o id do time 1 no quarto confronto (posição 1) no lugar do times_id
                            $time1['times_id'] = $TimeID1;  
                             //variavel que atualiza o registro ($r2) com a variavel que armazena o id na coluna times_id
                            $r2->update($time1);
                    }else
                    if($resto == 3){
                            //variavel que pega o confronto do id informado
                            $r3 = Confronto::find($idp3);
                            // coloca o id do time 1 no quarto confronto (posição 2) no lugar do times_id
                            $time2['times_id2'] = $TimeID1;  
                             //variavel que atualiza o registro ($r2) com a variavel que armazena o id na coluna times_id
                            $r3->update($time2);
                    }
                }
                    
            // se a soma do time 2 for maior que do time 1
            }else{       
                //coloca o id na coluna vencedor
                $venc['vencedor'] = $TimeID2;  
                //variavel que pega o confronto do id informado
                $reg = Confronto::find($id);
                //variavel que atualiza o registro ($reg) com a variavel que armazena o id na coluna vencedor
                $v = $reg->update($venc);

                $ultimoVencedor = $c[3]->times_id2;

                if($v){
                    if($resto == 1){
                        // variavel que pega o confronto do id informado
                        $reg = Confronto::find($idp2);
                         // coloca o id do time 1 no terceiro confronto (posição 2) no lugar do times_id2
                        $time2['times_id2'] = $TimeID2;
                        //variavel que atualiza o registro ($reg) com a variavel que armazena o id na coluna vencedor
                        $reg->update($time2);   
                    } else
                    if($resto == 2){
                            //variavel que pega o confronto do id informado
                            $r2 = Confronto::find($idp3);
                            // coloca o id do time 1 no quarto confronto (posição 1) no lugar do times_id
                            $time1['times_id'] = $TimeID2;  
                             //variavel que atualiza o registro ($r2) com a variavel que armazena o id na coluna times_id
                            $r2->update($time1);
                    }else
                    if($resto == 3){
                            //variavel que pega o confronto do id informado
                            $r3 = Confronto::find($idp3);
                            // coloca o id do time 1 no quarto confronto (posição 2) no lugar do times_id
                            $time2['times_id2'] = $TimeID2;  
                             //variavel que atualiza o registro ($r2) com a variavel que armazena o id na coluna times_id
                            $r3->update($time2);                

                    }
                } 

            }

            if($ultimoVencedor > 0){
                $camp['times_id'] = $ultimoVencedor;
                $camp['jogos_id'] = $jid;

                Campeao::create($camp);
            }

            //variavel que pega o confronto do id informado
            $reg = Confronto::find($id);
            //variavel que armazena todos os dados de entrada
            $dados['status'] = $status;
            $dados['times_id'] = request()->input('times_id');
            $dados['times_id2'] = request()->input('times_id2');
            $dados['placar1'] = $placar1;
            $dados['placar2'] = $placar2;
            $dados['subPlacar1'] = $subPlacar1;
            $dados['subPlacar2'] = $subPlacar2;

                //variavel que atualiza o registro ($reg) com a variavel que armazena otodos os dados de entrada
            $alt = $reg->update($dados);

            //se a variavel da atualização foi executada
                if ($alt) {
                    //retorna para o admin/index com a mensagem de alteração
                    return redirect('admin/index')
                                    ->with('status', 'Confronto Alterado!');
                }

        //se não for igual a encerrado
        }else{
           //variavel que pega o confronto do id informado
           $reg = Confronto::find($id);
           //variavel que armazena todos os dados de entrada
           $dados = $request->all();
            //variavel que atualiza o registro ($reg) com a variavel que armazena otodos os dados de entrada
           $alt = $reg->update($dados);
           //se a variavel da atualização foi executada
            if ($alt) {
                //retorna para o admin/index com a mensagem de alteração
                return redirect('admin/index')
                                ->with('status', 'Confronto Alterado!');
            }          
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destacar ($id){
        //pega o confronto correspondente ao id
        $reg = Confronto::find($id);

        $reg->destaque = ($reg->destaque == 0) ? 1 : 0;
        $estado = ($reg->destaque == 1) ? 'Destacado' : 'Normalizado';

        $reg->save();

        if($reg){
            return redirect('admin/index')->with('status', $estado . ' !');
        }
    }

    public function cadBar(FormProdutosRequest $request){
        //pega os dados informados 
        $dados = $request->all();
        $dados['users_id'] = auth()->user()->id;
        // pega o nome informado
        $prodN = $request->nome;
        //verifica se nome do produto existe
        $nome = Bar::where('nome', $prodN)->get();

        $prod = Bar::create($dados);
            if($prod){
                    //retorna para o admin/index com a mensagem de cadastro
                    return redirect('admin/index')->with('status', 'Produto Cadastrado');
       }
    }

    public function excluirBar($id)
    {
        $bars = Bar::find($id);
        if ($bars->delete()) {
            return redirect('admin/index')
                ->with('status', 'Produto Excluído!');
        }
    }

    public function cadProg(Request $request){
        //pega os dados informados 
        $dados = $request->all();
        $dados['users_id'] = auth()->user()->id;
        // pega o texto informado
        $progT = $request->nome;
        //verifica se o texto da prog existe
        $texto = Programacao::where('texto', $progT)->get();

        $prog = Programacao::create($dados);
            if($prog){
                    //retorna para o admin/index com a mensagem de cadastro
                    return redirect('admin/index')->with('status', 'Programação Cadastrada');
       }
    }

    public function excluirProg($id)
    {
        $programacaos = Programacao::find($id);
        if ($programacaos->delete()) {
            return redirect('admin/index')
                ->with('status', 'Programação Excluída!');
        }
    }
}
