<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use App\Http\Requests\FormRegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
        //
    }

    public function store(FormRegisterRequest $request)
    {
        // echo $request->name;
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        
        if($user){
            return redirect('/')->with('status', 'Registrado com sucesso!');

        }else{
              return redirect('/')->with('statusErr', 'Erro ao Registrar!');
        }
    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
