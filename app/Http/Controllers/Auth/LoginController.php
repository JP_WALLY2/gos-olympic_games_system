<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logar(Request $request){

        $user = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
           );
           //pega o email informado
           $dadosUe = $request->email;
           //pega a senha informada
           $dadosUs = $request->senha;
           //pega o email informado do banco se existir
           $usE= User::where('email', $dadosUe)->get();
           //pega a senha se existir do email informado do banco se existir
           $usS = User::where('email', $dadosUe)->where('password', $dadosUs)->get();

           if(Auth::attempt($user)){
            return redirect('admin/index')->with('status', 'Login realizado com sucesso.');
           }else{
            if ($usE->count()<=0 || $usS->count()<=0){
                return redirect('/')->with('err', 'Email ou senha incorretos.')->with('statusErr', 'Erro ao Logar.');
            }
           }
    }
    
    function sair()
    {
     Auth::logout();
     return redirect('/')->with('status', 'Logout realizado com Sucesso.');
    }
}
