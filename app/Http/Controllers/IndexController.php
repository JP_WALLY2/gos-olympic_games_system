<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jogo;
use App\Confronto;
use App\Time;
use App\Bar;
use App\Programacao;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //variavel com todos os confrontos
        $confrontos = Confronto::where('destaque', '1')->get();

        //variavel com os times por nome
        $times = Time::orderBy('nome')->get();
        
        $jogos = Jogo::orderBy('nome')->get();
        
        $allConf = Confronto::all();

        $bars = Bar::all();

        $programacoes = Programacao::all();

        return view('index',
        [
            'confrontos' =>$confrontos,
            'allConf' =>$allConf,
            'times' =>$times,
            'jogos' =>$jogos,
            'bars'  =>$bars,
            'programacoes' =>$programacoes

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
