<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormProdutosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'                  => 'required|unique:bars',
            'preco'                  => 'required',
        ];
    }

    public function messages(){

        return [
            'nome.required' => 'O campo Nome é obrigatório.',
            'nome.unique'   => 'Produto já existe.',

            'preco.required'   => 'O campo Preço é obrigatório.'
        ];
    }
}
