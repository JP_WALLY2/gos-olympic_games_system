<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'                  => 'required|unique:users|min:3',
            'email'                 => 'required|unique:users',
            'password'              => 'required|min:6',
            // 'password'              => 'required|confirmed|min:6',
            'passwordConfirm'       => 'required|same:password'
        ];
    }

    public function messages(){

        return [
            'name.required' => 'O campo Nome é obrigatório.',
            'name.unique'   => 'Nome já registrado.',
            'name.min'      => 'O Nome deve ter no mínimo 3 caracteres.',

            'email.required' => 'O campo Email é obrigatório.',
            'email.unique'   => 'Email já registrado.',

            'password.required'  => 'O campo Senha é obrigatório.',
            // 'password.confirmed' => 'A Senha deve ser confirmada.',
            'password.min'       => 'A Senha deve ter no mínimo 6 caracteres.',
            
            'passwordConfirm.required'  => 'O campo Confirmar Senha é obrigatório.',
            'passwordConfirm.same'      => 'A confirmação da senha e a senha devem ser iguais.'
        ];
    }
}
