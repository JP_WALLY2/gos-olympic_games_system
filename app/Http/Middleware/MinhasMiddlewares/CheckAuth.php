<?php

namespace App\Http\Middleware\MinhasMiddlewares;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::check()){

            return redirect('/')->with('status', 'Página acessível somente por administradores.');
        }
        return $next($request);
    }
}
