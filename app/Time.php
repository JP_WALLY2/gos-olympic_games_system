<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $fillable = ['nome', 'img', 'vitoria'];

 public function confrontos() {
 	return $this->hasMany('App\Confronto', 'confronto_id', 'id');
}

 public function campeaos() {
 	return $this->hasMany('App\Campeao', 'campeao_id', 'id');
}

}
