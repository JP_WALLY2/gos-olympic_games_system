<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campeao extends Model
{
    protected $fillable = ['times_id', 'jogos_id'];

    public function times() {
        return $this->belongsTo('App\Time', 'times_id', 'id');
    }

    public function jogos() {
        return $this->belongsTo('App\Jogo', 'jogos_id', 'id');
    }
}
