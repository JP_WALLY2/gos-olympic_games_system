<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Olimpíadas JEPAP - Administrador</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    
    {{--  <link rel="stylesheet" href="css/bootstrap.min.css">  --}}
    
    <link rel="stylesheet" href=" {{asset('css/stylesAdmin.css')}}">
    <link rel="stylesheet" href=" {{asset('css/mediaQueryAdmin.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg nav-principalAdmin d-flex fixed-top shadow">
      <img src="../../img/jepapV.png" id="imgLogo" alt="JEPAP"  width="80" height="80" class=" flex-grow-2 ml-2" style="margin-bottom: 8px">
      <a class="flex-grow-1"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <div class="collapse navbar-collapse flex-grow-0" id="navbarNavAltMarkup">
        <div class="navbar-nav text-center" id="mainNav">
          <a class="nav-link p-3 h2" href="#conf">Confrontos</a>
          <a class="nav-link p-3 h2" href="#bar">Bar</a>
          <a class="nav-link p-3 h2" href="#prog">Programação</a>
          
          @if(Auth::user())
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle py-4 h4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item text-center" href="{{ route('/') }}">Voltar</a>
              
              <a class="dropdown-item text-center" href="{{ route('logoutUser') }}"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              Sair
            </a>
            
            <form id="logout-form" action="{{ route('logoutUser') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </div>
        </li>
        @endif
      </nav>
      
      @if (session('status'))
      <div id="msg" class="alert alert-success container w-75 d-flex justify-content-center text-center py-2 px-5 d-flex fixed-top mb-5 msg">
        <h4>{{ session('status') }} </h4>
      </div>
      @elseif(session('StatusErr'))
      <div id="msg" class="alert alert-danger container w-75 d-flex justify-content-center text-center py-2 px-5 d-flex fixed-top mb-5 msg">
        <h4>{{ session('StatusErr') }} </h4>
      </div>
      @endif
      
      <section id="conf" class="container py-5">
        <div class="row mt-5">
          <div class="col-10 col-lg-12 inicio-item mx-auto mt-3">
            <h1 class="my-1 text-center text-white text-uppercase confronto">Confrontos</h1>
            
            <form method="POST" action="{{ route('index.str') }}">
              
              @csrf
              
              <div class="d-flex justify-content-center">
                  <div class="form-group d-flex justify-content-center mt-0 mb-lg-2 mb-1" >
                    <label for="jogo" class="col-12 col-lg-12 col-form-label labels jj">Jogo</label>
                  </div>
                </div>
                <div class="d-flex justify-content-center">
                  <div class="form-group d-flex justify-content-center">
                    <div class="col-12">
                      <select id="jogos_id" class="form-control" name="jogos_id">
                        <option class="dropdown-item ">Selecionar Jogo</option>
                        @foreach ($jogos as $j)
                        <option class="dropdown-item" value="{{$j->id}}"
                          {{ ((isset($reg) and $reg->jogos_id == $j->id) or
                            old('jogos_id') == $j->id) ? 'selected' : ''}}>
                            {{$j->nome}} {{$j->categoria}}</option> 
                            @endforeach
                          </select>
                          <strong>{{ session('errj') }}</strong>
                        </div>
                      </div>
                    <div>
                      
                    </div>
                  </div>
                  
                  
                  <div class="container cont">
                    <div class="d-flex justify-content-center">
                      <h2 class="text-uppercase fase">Mata-Mata</h2>
                    </div>
                    
                    <div class="d-flex justify-content-center">
                      <div class="row">
                        <div class="d-inline-flex justify-content-center">
                          <div>
                            <div class="form-group d-flex justify-content-center my-1">
                              <label for="time1_id" class="col-6 col-lg-6 col-form-label labels">Time</label>
                            </div>
                            <div class="form-group d-flex justify-content-center">
                              <div class="col-10 col-lg-12">
                                <select id="time1_id" class="form-control selectTime" name="time1_id">
                                  <option value="">Selecionar Time</option>
                                  @foreach ($times as $t)
                                  <option class="dropdown-item" value="{{$t->id}}"
                                    {{ ((isset($reg) and $reg->times_id == $t->id) or
                                      old('times_id') == $t->id) ? 'selected' : ''}}>
                                      {{$t->nome}}</option> 
                                      @endforeach
                                    </select>
                                    <strong>{{ session('err') }}</strong>
                                  </div>
                                </div>
                              </div>
                              
                              <h2 class="x mt-3">x</h2>  
                              <div>
                                <div class="form-group d-flex justify-content-center my-1">
                                  <label for="time2" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                </div>
                                <div class="form-group d-flex justify-content-center">
                                  <div class="col-10 col-lg-12">
                                    <select id="time2_id" class="form-control selectTime" name="time2_id">
                                      <option value="">Selecionar Time</option>
                                      @foreach ($times as $t)
                                      <option class="dropdown-item" value="{{$t->id}}"
                                        {{ ((isset($reg) and $reg->times_id == $t->id) or
                                          old('times_id') == $t->id) ? 'selected' : ''}}>
                                          {{$t->nome}}</option> 
                                          @endforeach
                                        </select>
                                        <strong>{{ session('err') }}</strong>
                                      </div>
                                    </div>         
                                  </div>   
                                </div>     
                                
                                <div class="d-inline-flex justify-content-center resp">
                                  <div class="ml-lg-5">
                                    <div class="form-group d-flex justify-content-center my-1">
                                      <label for="time3" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                    </div>
                                    <div class="form-group d-flex justify-content-center">
                                      <div class="col-10 col-lg-12">
                                        <select id="time3_id" class="form-control selectTime" name="time3_id">
                                          <option value="">Selecionar Time</option>
                                          @foreach ($times as $t)
                                          <option class="dropdown-item" value="{{$t->id}}"
                                            {{ ((isset($reg) and $reg->times_id == $t->id) or
                                              old('times_id') == $t->id) ? 'selected' : ''}}>
                                              {{$t->nome}}</option> 
                                              @endforeach
                                            </select>
                                            <strong >{{ session('err') }}</strong>
                                          </div>
                                        </div>
                                      </div>
                                      <h2 class="x mt-3">x</h2>  
                                      <div>
                                        <div class="form-group d-flex justify-content-center my-1">
                                          <label for="time4" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                        </div>
                                        <div class="form-group d-flex justify-content-center">
                                          <div class="col-10 col-lg-12">
                                            <select id="time4_id" class="form-control selectTime" name="time4_id">
                                              <option value="">Selecionar Time</option>
                                              @foreach ($times as $t)
                                              <option class="dropdown-item" value="{{$t->id}}"
                                                {{ ((isset($reg) and $reg->times_id == $t->id) or
                                                  old('times_id') == $t->id) ? 'selected' : ''}}>
                                                  {{$t->nome}}</option> 
                                                  @endforeach
                                                </select>
                                                <strong>{{ session('err') }}</strong>
                                                
                                              </div>
                                            </div>         
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <div class="d-flex justify-content-lg-around" style="margin-top: -20px">
                                      <h2 class="text-uppercase fase sf d-lg-block d-none">Semi-Final</h2>
                                      <h2 class="text-uppercase fase f d-lg-block d-none">Final</h2>
                                    </div>
                                    
                                    <div class="d-flex justify-content-center">
                                      <h2 class="text-uppercase fase sf d-block d-lg-none">Semi-Final</h2>
                                    </div>
                                    
                                    <div class="d-flex justify-content-center  mb-3">
                                      <div class="row">
                                        <div class="d-inline-flex justify-content-center">
                                          <div>
                                            <div class="form-group d-flex justify-content-center my-1">
                                              <label for="time5" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                            </div>
                                            <div class="form-group d-flex justify-content-center">
                                              <div class="col-10 col-lg-12">
                                                <select id="time5_id" class="form-control selectTime" name="time5_id">
                                                  <option value="">Selecionar Time</option>
                                                  @foreach ($times as $t)
                                                  <option class="dropdown-item" value="{{$t->id}}"
                                                    {{ ((isset($reg) and $reg->times_id == $t->id) or
                                                      old('times_id') == $t->id) ? 'selected' : ''}}>
                                                      {{$t->nome}}</option> 
                                                      @endforeach
                                                    </select>
                                                    <strong>{{ session('err') }}</strong>
                                                  </div>
                                                </div>
                                              </div>
                                              
                                              <h2 class="x mt-3">x</h2>  
                                              <div>
                                                <div class="form-group d-flex justify-content-center my-1">
                                                  <label for="time6" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                                </div>
                                                <div class="form-group d-flex justify-content-center">
                                                  <div class="col-10 col-lg-12">
                                                    <select id="time6_id" class="form-control desativado" disabled name="time6_id">
                                                      <option value="0">Vencedor Conf. 2 </option>
                                                    </select>
                                                  </div>
                                                </div>         
                                              </div>  
                                            </div>   
                                            
                                            <div class="d-flex justify-content-center jcc">
                                              <h2 class="text-uppercase fase f d-block d-lg-none">Final</h2>
                                            </div>  
                                            
                                            <div class="d-inline-flex justify-content-center">
                                              <div class="ml-lg-5">
                                                <div class="form-group d-flex justify-content-center my-1">
                                                  <label for="time7" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                                </div>
                                                <div class="form-group d-flex justify-content-center">
                                                  <div class="col-10 col-lg-12">
                                                    <select id="time7_id" class="form-control desativado" name="time7_id" disabled>
                                                      <option value="0">Vencedor Conf. 1</option>
                                                    </select>
                                                  </div>
                                                </div>
                                              </div>
                                              <h2 class="x mt-3">x</h2>  
                                              <div>
                                                <div class="form-group d-flex justify-content-center my-1">
                                                  <label for="time8" class="col-6 col-lg-6 col-form-label labels">Time</label>
                                                </div>
                                                <div class="form-group d-flex justify-content-center">
                                                  <div class="col-10 col-lg-12">
                                                    <select id="time8_id" class="form-control desativado" name="time8_id" disabled>
                                                      <option value="0">Vencedor Conf. 3</option>
                                                    </select>
                                                  </div>
                                                </div>         
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="d-flex justify-content-center py-2">
                                            <button type="submit" class="btn btn-cad text-uppercase px-4 mb-1">
                                              Cadastrar
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div> 
                              </section>
                              
                              @php
                              $numJogos = sizeof($confrontos) / 4;
                              $indexConf = 0;
                              @endphp
                              
                              <section id="tabelas" class="container px-0">
                                <div id="slider" class="carousel slide" data-ride="corousel">
                                  <div class="carousel-inner">
                                    @for ($i = 0; $i < $numJogos; $i++)

                                    @php
                                    $idjogo = $confrontos[$indexConf]->jogos_id;
                                    @endphp

                                    @if ($i == 0)
                                    <div class="carousel-item active">
                                      @else
                                      <div class="carousel-item">
                                        @endif
                                        <div class="container">
                                          <div class="row">
                                            <div class="col-10 col-lg-12 inicio-item mx-auto">
                                              <h1 class="text-center text-uppercase my-2 text-white">
                                                {{$confrontos[$indexConf]->jogos->nome}}

                                               
                                                <strong style="color: #0BB511" class="text-capitalize">
                                                @php
                                                    $cat = $confrontos[$indexConf]->jogos->categoria;
                                                    if ($cat != null) {

                                                     echo '(' . $cat . ')';
                                                    }                             
                                                @endphp
                                                </strong>
                                              </h1>
                                              <div>
                                                
                                                <div class="table-striped table-responsive">
                                                  <table class="inicio-item table table-borderless ">
                                                    {{-- CABEÇALHO DA TABELA --}}
                                                    <thead class="text-center h3 text-uppercase text-white">

                                                      {{-- TAMANHO GRANDE --}}
                                                      <tr class="cabecalho">
                                                          <th title="Confronto por {{$confrontos[$indexConf]->jogos->pontuacao}}">
                                                          Confrontos</th>
                                                          <th title="Status">Status</th>
                                                          <th title="Fase">Fase</th>
                                                          <th title="Vencedor">Vencedor</th>
                                                          <th title="Cadastrado por">C/ Por</th>
                                                          <th></th>
                                                          <th></th>
                                                      </tr>
                                                        {{-- FIM TAMANHO GRANDE --}}

                                                        {{-- TAMANHO PEQUENO --}}
                                                       

                                                      {{-- FIM TAMANHO PEQUENO --}}
                                                    </thead>
                                                    
                                                    {{-- FIM DO CABEÇALHO DA TABELA --}}

                                                    {{-- INICIO CORPO DA TABELA --}}
                                                    <tbody class="text-center text-white text-capitalize">
                                                      @for ($j = 0; $j < 4; $j++)
                                                      <tr class="corpo">
                                                        
                                                          {{-- TAMANHO GRANDE --}}
                                                        <td>
                                                          <form method="POST" action="{{ route ('index.update', $confrontos[$indexConf]->id)}}" enctype="multipart/form-data">
                                                            {!! method_field('put') !!}
                                                            {{ csrf_field() }}
                                                        @if ($confrontos[$indexConf]->times != null)
                                                          <input type="hidden" name="jogos_id" value="{{$confrontos[$indexConf]->jogos_id}}">
                                                          <input type="hidden" name="times_id" value="{{$confrontos[$indexConf]->times_id}}">
                                                          <input type="hidden" name="vencedor" value="{{$confrontos[$indexConf]->vencedor}}">

                                                          {{$confrontos[$indexConf]->times->nome}}
                                                          <div id="divP1_{{$indexConf}}"
                                                          style="display: inline-block;">
                                                          {{$confrontos[$indexConf]->placar1}}

                                                          @if ($confrontos[$indexConf]->subPlacar1 > 0)
                                                            ({{$confrontos[$indexConf]->subPlacar1}})    
                                                          @else
                                                            {{$confrontos[$indexConf]->subPlacar1}}  
                                                          @endif
                                                        </div>
                                                        @else
                                                              
                                                          Vencedor 
                                                          <div id="divP1_{{$indexConf}}"
                                                          style="display: inline-block;">
                                                          {{$confrontos[$indexConf]->placar1}}
                                                          {{$confrontos[$indexConf]->subPlacar1}}
                                                          </div>
                                                          
                                                          @endif

                                                          <input value="{{$confrontos[$indexConf]->placar1}}" name="placar1" id="inputs_{{$indexConf}}_1" type="number" style=" width: 30px; display: none" min="0">
                                                          @if($confrontos[$indexConf]->jogos->id < 5)
                                                            <input  value="{{$confrontos[$indexConf]->subPlacar1}}" name="subPlacar1" id="inputs_{{$indexConf}}_3" type="number" style="border-radius: 10px; width: 25px; display: none" min="0">
                                                          @endif
                                                          x
                                                          @if($confrontos[$indexConf]->jogos->id < 5)
                                                            <input  value="{{$confrontos[$indexConf]->subPlacar2}}" name="subPlacar2" id="inputs_{{$indexConf}}_4" type="number" style="border-radius: 10px; width: 25px; display: none" min="0">
                                                          @endif
                                                          <input  value="{{$confrontos[$indexConf]->placar2}}" name="placar2" id="inputs_{{$indexConf}}_2" type="number" style=" width: 30px; display: none" min="0">
                                                          
                                                          @if ($confrontos[$indexConf]->times2 != null)
                                                          <input type="hidden" name="times_id2" value="{{$confrontos[$indexConf]->times_id2}}">
                                                            <div id="divP2_{{$indexConf}}" style="display: inline-block;">

                                                              @if ($confrontos[$indexConf]->subPlacar2 > 0)
                                                            ({{$confrontos[$indexConf]->subPlacar2}})    
                                                          @else
                                                            {{$confrontos[$indexConf]->subPlacar2}}  
                                                          @endif
                                                                  
                                                              {{$confrontos[$indexConf]->placar2}}
                                                            </div>
                                                          {{$confrontos[$indexConf]->times2->nome}}
                                                          @else
                                                            <div id="divP2_{{$indexConf}}" style="display: inline-block;">
                                                              {{$confrontos[$indexConf]->subPlacar2}}
                                                              {{$confrontos[$indexConf]->placar2}}
                                                            </div>
                                                          Vencedor
                                                          @endif
                                                          </td>
                                                          <td >
                                                            <div id="tabStatus_{{$indexConf}}">
                                                              @if ($confrontos[$indexConf]->status == "andamento")
                                                              {{ "Em Andamento" }}
                                                              @else
                                                              {{$confrontos[$indexConf]->status}}
                                                              @endif
                                                            </div>
                                                            <select id="selectStatus_{{$indexConf}}" name="status" class="form-control" style="display: none; height: 30px; width: 150px">
                                                              <option value="andamento" >Em Andamento</option>
                                                              <option value="pausado" >Pausado</option>
                                                              <option value="encerrado" >Encerrado</option>  
                                                            </select>
                                                          </td>
                                                          <td >
                                                            {{$confrontos[$indexConf]->fase}}
                                                          </td>
                                                          <td >
                                                           
                                                            @if ($confrontos[$indexConf]->vencedor == null)
                                                              Não Definido
                                                            @else
                                                              @if ($confrontos[$indexConf]->vencedor == $confrontos[$indexConf]->times_id)
                                                                {{$confrontos[$indexConf]->times->nome}}
                                                              @elseif($confrontos[$indexConf]->vencedor == $confrontos[$indexConf]->times_id2)
                                                                {{$confrontos[$indexConf]->times2->nome}}
                                                              @endif
                                                            @endif
                                                          </td>
                                                          <td >
                                                            {{$confrontos[$indexConf]->users->name}}
                                                          </td>
                                                         
                                                          {{-- FIM TAMANHO GRANDE --}}

                                                          {{-- TAMANHO PEQUENO --}}

                                                         
                                                        {{-- FIM TAMANHO PEQUENO --}}
                                                       
                                                        <td class="dlgnone" aria-disabled="true">
                                                          <label class="container">
                                                              <input type="checkbox" name="destacar" value="" id="a_{{$confrontos[$indexConf]->id}}"
                                                              onclick="window.location.href='{{ route ('index.destacar', $confrontos[$indexConf]->id)}}'" 
                                                              >
                                                              <span class="checkmark" title="Destacar"
                                                              @if ($confrontos[$indexConf]->destaque == 1)
                                                              style="background-color: #0BB511; border: none;"
                                                              @else
                                                              style="background-color: rgba(0, 0, 0, 0.6);"
                                                              @endif
                                                              ></span>
                                                          </label>
                                                        </td>
                                                        <td class="d-flex">
                                                          <input type="hidden">
                                                          <button id="a_{{$indexConf}}" class="btn btn-success btn-sm" type="submit" title="Cadastrar" style="display: none">
                                                         <i class="fas fa-pencil-alt"></i>
                                                          </button>
                                                          &nbsp;
                                                        </form>
                                                         
                                                        @if ($confrontos[$indexConf]->status != "encerrado" && $confrontos[$indexConf]->times_id2 != null)
                                                        <a  class="btn btn-warning btn-sm" title="Editar" onclick="abrirEditor({{$indexConf}})"
                                                          role="button"><i class="fa fa-edit fa-xs"></i></a>           
                                                        @endif
                                                          </td>
                                                        </tr>
                                                        @php
                                                        $indexConf++
                                                        @endphp
                                                        @endfor
                                                      </tbody>
                                                      <tbody class="text-center text-white">
                                                        <tr>
                                                          @foreach ($campeaos as $camp)
                                                          
                                                          @if ($camp->jogos_id == $idjogo)
                                                           <td colspan=8 >Vitória do <strong style="color: #0BB511"> {{$camp->times->nome}} </strong> </td>  
                                                          @else
                                                            <td colspan=8 >Ainda não há um vencedor</td>  
                                                          @endif
                                                          @endforeach
                                                        </tr>
                                                      </tbody>
                                                      {{-- FIM DO CORPO DA TABELA --}}
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                        @endfor
                                      </div>
                                      <a href="#slider" class="carousel-control-prev d-inline-block prev-left" role="button" data-slide="prev">
                                        <i class="fas fa-chevron-left fa-3x"></i></a>
                                        <a href="#slider" class="carousel-control-next d-inline-block prev-right" role="button" data-slide="next">
                                          <i class="fas fa-chevron-right fa-3x"></i></a>
                                        </div>
                                      
                                      </section>

                                      <section id="bar" class="container py-3">
                                        <div class="row">
                                          <div class="col-10  col-lg-12 inicio-item mx-auto mt-3">
                                            <h1 class="my-1 text-center text-white confronto">Cadastro do Bar</h1>
                                            <form method="POST" action="{{ route ('index.cadBar')}}"  enctype="multipart/form-data">
                                              @csrf
                                              <div class="d-flex justify-content-center">
                                                <div class="row">
                                                  <div class="d-inline-flex justify-content-center">
                                                    <div>
                                                      <div class="form-group d-flex justify-content-center my-1">
                                                        <label for="nome" class="col-6 col-lg-6 col-form-label labels">Produto</label>
                                                      </div>
                                                      <div class="form-group d-flex justify-content-center">
                                                        <div class="col-10 col-lg-12">
                                                          <input type="text" value="" id="nome" name="nome" class="form-control">
                                                          <strong>{{ $errors->first('nome') }}</strong>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        
                                                        <div>
                                                          <div class="form-group d-flex justify-content-center my-1">
                                                            <label for="preco" class="col-6 col-lg-6 col-form-label labels">Preço(R$)</label>
                                                          </div>
                                                          <div class="form-group d-flex justify-content-center">
                                                            <div class="col-10 col-lg-12">
                                                              <input type="text" value="" id="preco" name="preco" class="form-control preco">
                                                              <strong>{{ $errors->first('preco') }}</strong>
                                                                </div>
                                                              </div>         
                                                            </div>   
                                                          </div>     
                                                        </div>
                                                      </div>

                                                            <div class="form-group">
                                                              <div class="d-flex justify-content-center pt-4 pb-0 mb-0">
                                                                <button id="btns" type="submit" class="btn btn-cad text-uppercase px-4 mb-1">
                                                                  Cadastrar
                                                                </button>
                                                              </div>
                                                            </div>
                                                          </form>

                                                            <div class="table-striped table-responsive">
                                                              <table class="table inicio-item table-borderless" id="example">
                                                                <thead class="text-center h3 text-uppercase text-white">
                                                                  <tr>
                                                                    <th title="Produto">Produto</th>
                                                                    <th title="Preço">Preço (R$)</th>
                                                                    <th class="d-none d-lg-block" title="Cadastrado por">Cadastrado Por</th>
                                                                    <th></th>
                                                                  </tr>
                                                                </thead>
                                                                
                                                                <tbody class="text-center text-white text-capitalize">
                                                                  @forelse ($bars as $b)
                                                                  <tr>
                                                                    <td>{{$b->nome}}</td>
                                                                    <td>{{number_format($b->preco, 2, ',', '.')}}</td>
                                                                    <td class="d-none d-lg-block">{{$b->users->name}}</td>
                                                                    <td>
                                                                      <form style="display: inline-block"
                                                                      method="post"
                                                                      action="{{route('index.excluirBar', $b->id)}}"
                                                                      onsubmit="return confirm('Confirma Exclusão?')">
                                                                       {{method_field('delete')}}
                                                                       {{csrf_field()}}
                                                                      <button type="submit" title="Excluir" style="background-color: #ff2400"
                                                                              class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                                                      </form>
                                                                    </td>
                                                                  </tr>
                                                                  @empty
                                                                    <tr><td colspan=8> Não há produtos cadastrados </td></tr>
                                                                  @endforelse
                                                                </tbody>
                                                              </table>
                                                              {{ $bars->links('layouts.paginate') }}
                                                            </div>
                                          </div>
                                        </div>
                                      </section>


                                      <section id="prog" class="container py-3 mb-5 ">
                                        <div class="row">
                                          <div class="col-10  col-lg-12 inicio-item mx-auto mt-3">
                                            <h1 class="my-1 text-center text-white confronto">Cadastro da Programação</h1>
                                            <form method="POST" action="{{ route ('index.cadProg')}}"  enctype="multipart/form-data">
                                              @csrf
                                              <div class="d-flex justify-content-center">
                                                <div class="row">
                                                  <div class="d-inline-flex justify-content-center">
                                                    <div>
                                                      <div class="form-group d-flex justify-content-center my-1">
                                                        <label for="texto" class="col-6 col-lg-6 col-form-label labels">Programação</label>
                                                      </div>
                                                      <div class="form-group d-flex justify-content-center">
                                                        <div class="col-10 col-lg-12">
                                                          <input type="text" value="" id="texto" name="texto" class="form-control">
                                                          <strong>{{ $errors->first('texto') }}</strong>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        
                                                        <div>
                                                          <div class="form-group d-flex justify-content-center my-1">
                                                            <label for="hora" class="col-6 col-lg-6 col-form-label labels">Hora</label>
                                                          </div>
                                                          <div class="form-group d-flex justify-content-center">
                                                            <div class="col-10 col-lg-12">
                                                              <input type="time" value="" id="hora" name="hora" class="form-control">
                                                              <strong>{{ $errors->first('hora') }}</strong>
                                                                </div>
                                                              </div>         
                                                            </div>   
                                                          </div>     
                                                        </div>
                                                      </div>

                                                            <div class="form-group">
                                                              <div class="d-flex justify-content-center pt-4 pb-0 mb-0">
                                                                <button type="submit" class="btn btn-cad text-uppercase px-4 mb-1">
                                                                  Cadastrar
                                                                </button>
                                                              </div>
                                                            </div>
                                                          </form>
                                                            <div class="table-striped table-responsive">
                                                              <table class="table inicio-item table-borderless">
                                                                <thead class="text-center h3 text-uppercase text-white">
                                                                  <tr>
                                                                    <th title="Hora">Hora</th>
                                                                    <th title="Programação">Programação</th>
                                                                    <th class="d-none d-lg-block" title="Cadastrado por">Cadastrado Por</th>
                                                                    <th></th>
                                                                  </tr>
                                                                </thead>
                                                                
                                                                <tbody class="text-center text-white text-capitalize tpg">
                                                                  @forelse ($programacao as $p)
                                                                  <tr>
                                                                    <td>{{$p->hora}}</td>
                                                                    {{-- <td>{{date_format($p->hora,"H:i")}}</td> --}}
                                                                    <td>{{$p->texto}}</td>
                                                                    <td class="d-none d-lg-block">{{$p->users->name}}</td>
                                                                    <td>
                                                                      <form style="display: inline-block"
                                                                      method="post"
                                                                      action="{{route('index.excluirProg', $p->id)}}"
                                                                      onsubmit="return confirm('Confirma Exclusão?')">
                                                                       {{method_field('delete')}}
                                                                       {{csrf_field()}}
                                                                      <button type="submit" title="Excluir" style="background-color: #ff2400"
                                                                              class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                                                      </form>
                                                                    </td>
                                                                  </tr>
                                                                  @empty
                                                                    <tr><td colspan=8> Não há programação </td></tr>
                                                                  @endforelse
                                                                </tbody>
                                                              </table>
                                                              {{ $bars->links('layouts.paginate') }}
                                                            </div>
                                          </div>
                                        </div>
                                      </section>
                                      
                                    </body>
                                    </html>
                  
<script src="/js/script.js"></script>
<script src="/'js/bootstrap.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>

<script>

  var input1Placar;
  var input2Placar;
  
  function abrirEditor(indexConf){
    
    tabStatus = document.getElementById('tabStatus_'+indexConf);
    divP1 = document.getElementById('divP1_' +indexConf);
    divP2 = document.getElementById('divP2_' +indexConf);

    aCadastrar = document.getElementById('a_'+indexConf);
    selectEstatus = document.getElementById('selectStatus_'+indexConf);
    input1Placar = document.getElementById('inputs_'+indexConf+'_1');
    input2Placar = document.getElementById('inputs_'+indexConf+'_2');
    input1subPlacar = document.getElementById('inputs_'+indexConf+'_3');
    input2subPlacar = document.getElementById('inputs_'+indexConf+'_4');
    
    if (input1Placar.style.display == 'none') {

      tabStatus.style.display = 'none';
      divP1.style.display = 'none';
      divP2.style.display = 'none';
      
      selectEstatus.style.display = 'inline-block';
      aCadastrar.style.display = 'inline-block';
      input1Placar.style.display = 'inline-block';
      input2Placar.style.display = 'inline-block';
      input1subPlacar.style.display = 'inline-block';
      input2subPlacar.style.display = 'inline-block';
    } else {
      tabStatus.style.display = 'inline-block';
      divP1.style.display = 'inline-block';
      divP2.style.display = 'inline-block';

      selectEstatus.style.display = 'none';
      aCadastrar.style.display = 'none';
      input1Placar.style.display = 'none';
      input2Placar.style.display = 'none';
      input1subPlacar.style.display = 'none';
      input2subPlacar.style.display = 'none';
    }
  }

</script>
