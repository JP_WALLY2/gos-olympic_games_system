<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Olimpíadas JEPAP</title>

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <link rel="stylesheet" href=" {{asset('css/styles.css')}}">
    <link rel="stylesheet" href=" {{asset('css/mediaQuery.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body> 

<nav class="navbar navbar-expand-lg nav-principal navbar-light d-flex fixed-top shadow">
  <a class="nav-link flex-grow-1 text-white text-uppercase h1 inicio" href="#inicio">Início</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse flex-grow-0" id="navbarNavAltMarkup">
<div class="navbar-nav text-center">
  @if(Auth::user() == false)
  <a class="nav-link p-3 h2" type="button" onclick="abrirModal(0), iniciarVarsFecharModal()" class="trigger-btn" data-toggle="modal">Login</a>
  @endif

  <a class="nav-link p-3 h2" href="#bar">Bar</a>
  <a class="nav-link p-3 h2" href="#jogos">Jogos</a>
  <a class="nav-link p-3 h2" href="#prog">Programação</a>

  @if(Auth::user())
  <li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle py-4 h4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ Auth::user()->name }}
    </a>
    <div class="dropdown-menu dropdown-menu-right">
      <a class="dropdown-item text-center" href="admin/index">Entrar</a>

        <a class="dropdown-item text-center" href="{{ route('logoutUser') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Sair
        </a>

        <form id="logout-form" action="{{ route('logoutUser') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>
@endif
</nav>

@if (session('status'))
    <div class="alert alert-success container w-75 d-flex justify-content-center text-center py-2 px-5 d-flex fixed-top mb-5 msg">
        <h4>{{ session('status') }} </h4>
    </div>
@elseif(session('statusErr'))
   <div class="alert alert-danger container w-75 d-flex justify-content-center text-center py-2 px-5 d-flex fixed-top mb-5 msg">
        <h4>{{ session('statusErr') }} </h4>
    </div>
@endif

<section id="inicio" class="container pt-5">
    <div id="slider" class="carousel slide" data-ride="corousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row mt-5 pt-4">
                        <div class="col-10 col-lg-12 inicio-item mt-3 mx-auto in">
                          <h1 class="titulo text-center mt-3 mt-lg-5">Bem Vindo as Olimpíadas JEPAP</h1>
                          <p class="text-white text-center">Aqui você acompanha os confrontos dos jogos, seus placares e o vencedor de cada jogo,
                             produtos que são vendidos no bar e a programação do dia.</p>
                        </div>
                    </div>
                </div>
            </div>
            @forelse ($confrontos as $c)
            <div class="carousel-item">
                <div class="container">
                    <div class="row mt-5 pt-4">
                        <div class="col-10 col-lg-12 inicio-item mt-3 mx-auto in">
                            <div class="inicio-status">
                              @if ($c->destaque == 1)
                                  <h1 class="status text-center text-uppercase">{{$c->status}}</h1>
                                  <h2 class="jogo text-center text-uppercase">{{$c->jogos->nome}} {{$c->jogos->categoria}}</h2>
                                  @else
                                  @endif
                                
                                <div class="row justify-content-center">
                                    <div class="mr-3">
                                        <img src="{{'../../' . $c->times->img}}" alt="{{ $c->times->nome}}" class="inicio-img">
                                          
                                        <div class="bg-texto-nome my-2 text-center py-2 my-lg-2 my-3">
                                          {{-- <div class="vit text-uppercase">vitória!</div> --}}  
                                          {{ $c->times->nome}}
                                        </div> 
                                    </div>
                                    <h1 class="h3 d-flex align-items-center">
                                        <div class="d-inline placar py-1 px-2 mr-2 ml-lg-5 px-lg-4">
                                          @if ($c->jogos->pontuacao == 'Vitórias')
                                            <h2 class="text-center text-capitalize pts">{{'Vit'}}</h2>
                                          @elseif ($c->jogos->pontuacao == 'Pontos')
                                            <h2 class="text-center text-capitalize pts">{{'Pts'}}</h2>
                                          @else
                                            <h2 class="text-center text-capitalize pts">{{$c->jogos->pontuacao}}</h2>
                                          @endif
                                          {{$c->placar1}}
                                          <h2 class="text-center text-capitalize pla">{{$c->subPlacar1}}</h2>

                                      </div>  
                                          X 
                                      <div class="d-inline placar py-1 px-2 ml-2 mr-lg-5 px-lg-4">
                                          @if ($c->jogos->pontuacao == 'Vitórias')
                                            <h2 class="text-center text-capitalize pts">{{'Vit'}}</h2>
                                          @else
                                            <h2 class="text-center text-capitalize pts">{{$c->jogos->pontuacao}}</h2>
                                          @endif
                                          {{$c->placar2}}
                                          <h2 class="text-center text-capitalize pla">{{$c->subPlacar2}}</h2>             
                                        </div>
                                    </h1>
                                    <div class="ml-3 ml-lg-5">
                                      <img src="{{'../../' . $c->times2->img}}" alt="{{ $c->times2->nome}}" class="inicio-img">
                                        
                                      <div class="bg-texto-nome my-2 text-center py-2 my-lg-2 my-3">
                                        {{-- <div class="vit text-uppercase">vitória!</div> --}}  
                                        {{ $c->times2->nome}}
                                      </div> 
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @empty 
                        @endforelse
        </div>
        
        <a href="#slider" class="carousel-control-prev  prev-left" role="button" data-slide="prev">
            <i class="fas fa-chevron-left fa-3x"></i></a>
        <a href="#slider" class="carousel-control-next  prev-right" role="button" data-slide="next">
            <i class="fas fa-chevron-right fa-3x"></i></a>
    </div>
</section>
        
        <section id="bar" class="container py-5">
            <div class="container">
                <div class="row">
                    <div class="col-10 col-lg-12 inicio-item mx-auto">
                        <h1 class="text-center text-uppercase my-2 text-white jogo">Bar</h1>
                        <table id="tableBar" class="inicio-item table table-borderless text-center h3 text-uppercase text-white">
                          <tr class="cabecalho">
                            <th>Produto</th>
                            <th>Preço</th>
                          </tr>
                          @forelse ($bars as $b)
                          <tr>
                            <td>{{$b->nome}}</td>
                            <td>{{$b->preco}}</td>
                          </tr>
                          @empty
                          <tr >
                            <td colspan="2" align="center">Não há produtos cadastrados</td>
                          </tr>
                          @endforelse
                          </table>
                    </div>
                </div>
            </div>
            </div>
        
    </section>
    
        <section id="jogos" class="container py-5">
            <div class="container">
                <div class="row">
                    <div class="col-10 col-lg-12 inicio-item mx-auto">
                        <h1 class="text-center text-uppercase my-2 text-white jogo">
                         Confrontos
                        </h1>
                        <div>
                        <table id="tableConf" class="inicio-item table table-borderless text-center h3 text-uppercase text-white position-sticky">
                            @forelse ($allConf as $allC)
                            <tr class="text-center text-white text-capitalize tc">
                              <td colspan="12">
                                <h2>
                                @if ($allC->times != null)
                                  {{$allC->times['nome']}}
                                @else
                                  Vencedor    
                                @endif
                                 <strong style="color: #0BB511">
                                   {{$allC->placar1}}
                                    @if ($allC->subPlacar1 > 0)
                                     ({{$allC->subPlacar1}})
                                    @endif
                                  </strong> 
                                        x
                                  <strong style="color: #0BB511">
                                    @if ($allC->subPlacar2 > 0)
                                      ({{$allC->subPlacar2}})
                                    @endif
                                      {{$allC->placar2}}
                                  </strong>
                                  @if ($allC->times2 != null)
                                    {{$allC->times2['nome']}}
                                  @else
                                  Vencedor    
                                  @endif
                                </h2>
                              </td>
                            </tr>
                              <tr class="text-center text-white text-capitalize">
                                <td >
                                  <div class="cabecalho p-1 mb-1">Jogo</div>
                                  {{$allC->jogos->nome}}</td>
                                
                                  <td><div class="cabecalho p-1 mb-1">Status</div>
                                    {{$allC->status}}</td>

                                  <td class="d-none d-lg-block">
                                    <div class="cabecalho p-1 mb-1">Fase</div>
                                    {{$allC->fase}}</td>

                                  <td>
                                    <div class="cabecalho p-1 mb-1">Vencedor</div>
                                    @if ($allC->vencedor == null)
                                      Não Definido
                                    @else
                                      @if ($allC->vencedor == $allC->times_id)
                                        {{$allC->times->nome}}
                                      @elseif($allC->vencedor == $allC->times_id2)
                                        {{$allC->times2->nome}}
                                      @endif
                                    @endif
                                  </td>   
                            @empty
                              <tr>
                                <td colspan="2" align="center">não há jogos cadastrada</td>
                              </tr>
                            @endforelse
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        
    </section>

    <section id="prog" class="container py-5">
      <div class="container">
          <div class="row">
              <div class="col-10 col-lg-12 inicio-item mx-auto">
                  <h1 class="text-center text-uppercase my-2 text-white jogo">Programação</h1>
                  <table id="tableProg" class="inicio-item table table-borderless text-center text-white text-capitalize table-sm">
                    @forelse ($programacoes as $p)
                    <tr>
                      {{--  <td>{{date_format($p->hora), 'H:i'}}</td>  --}}
                      <td>{{$p->hora}}</td>
                      <td>{{$p->texto}}</td>
                    </tr> 
                    @empty
                    <tr>
                      <td colspan="2" align="center">não há programação cadastrada</td>
                    </tr>
                    @endforelse    
                       
                    </table>
              </div>
          </div>
      </div>
    </div> 
</section>


<div id="loginModal" class="modal myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-login modal-tam my-4">
  <div class="row">
    <div class="col-12"> 
      <div id="painelLoginModal" class="modal-content border-0" style="background: rgba(0, 0, 0, 0.6)">
        <div class= "modal-bg-titulo modal-header text-uppercase justify-content-center 
        text-white border-0 rounded-0 py-1">Login</div>
        
        <div class="modal-body">
          <form method="POST" action="{{ route('loginUser') }}">
            @csrf
            
            <div class="form-group row d-inline-flex input-group">
              <label for="email" class="input-group-prepend modal-tam-label col-lg-4 col-form-label text-lg-right text-uppercase">EMail</label>
              <div class="col-lg-8">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                   <strong>{{ session('err') }}</strong> 
              </div>
            </div>
            
            <div class="form-group row d-inline-flex input-group">
              <label for="password" class="input-group-prepend modal-tam-label col-lg-4  col-form-label text-lg-right text-uppercase">Senha</label>
              <div class="col-lg-8">
                <input id="password" type="password" class="form-control" name="password" required>
              </div>
            </div>
            
            <div class="form-group">
              <div class="d-flex justify-content-center my-4">
                <a type="button" onclick="abrirModal(1), iniciarVars2FecharModal()" class="text-uppercase h4" class="trigger-btn" data-toggle="modal" style="color: white">
                  Registrar
                </a>
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-logar text-uppercase px-5">
                  Logar
                </button>
              </div>
            </div>
          </div> 
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="registerModal" class="modal myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-login modal-tam my-4">
  <div class="row">
    <div class="col-12"> 
      <div id="painelRegisterModal" class="modal-content border-0" style="background: rgba(0, 0, 0, 0.6)">
        <div class= "modal-bg-titulo modal-header text-uppercase justify-content-center 
        text-white border-0 rounded-0 py-1">Registrar</div>
        
        <div class="modal-body">
          <form method="POST" action="{{ route('newUser') }}">
            @csrf
            
            <div class="form-group row d-inline-flex input-group">
              <label for="name" class="input-group-prepend modal-tam-label col-lg-4 col-form-label text-lg-right text-uppercase">Nome</label>
              <div class="col-lg-8">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
                  
                  <strong>{{ $errors->first('name')}}</strong>
              </div>
            </div>
            
            <div class="form-group row d-inline-flex input-group">
              <label for="email" class="input-group-prepend modal-tam-label col-lg-4 col-form-label text-lg-right text-uppercase">EMail</label>
              <div class="col-lg-8">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                  <strong>{{ $errors->first('email')}}</strong>
              </div>
            </div>
            
            <div class="form-group row d-inline-flex input-group">
              <label for="password" class="input-group-prepend modal-tam-label col-lg-4  col-form-label text-lg-right text-uppercase">Senha</label>
              <div class="col-lg-8">
                <input id="password" type="password" class="form-control" name="password">
                  <strong>{{$errors->first('password') }}</strong>
              </div>
            </div>
            
            <div class="form-group row d-inline-flex input-group">
              <label for="passwordConfirm" class="input-group-prepend modal-tam-label col-lg-4  col-form-label text-lg-right text-uppercase" style="font-size: 12px; text-align: center;"> Confirmar Senha</label>
              <div class="col-lg-8">
                <input id="passwordConfirm" type="password" class="form-control" name="passwordConfirm">
                  <strong>{{$errors->first('passwordConfirm') }}</strong>
              </div>
            </div>
            
            <div class="form-group">
              <div class="d-flex justify-content-center">
                <a type="button" onclick="abrirModal(0)" class="text-uppercase h4" class="trigger-btn" data-toggle="modal" style="color: white">
                  Logar
                </a>
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-logar text-uppercase px-5">
                  Registrar
                </button>
              </div>
            </div>
          </form>
        </div> 
      </div>
    </div>
  </div>
</div>
</div> 

</body>

<script src="/js/script.js"></script>
<script src="/'js/bootstrap.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</html>
    <script>
    
      $('.nav-link').click(function(link){
        link.preventDefault();
        var target = $(this).attr('href');
        
        $('html, body').animate({
          scrollTop: $(target).offset().top - 80
        }, 1000);

      });

      setTimeout(function(){
        $('.msg').remove();
      }, 2500);

      var loginModal;
      var registerModal;

      function abrirModal(opcao){

        loginModal = document.getElementById('loginModal');
        registerModal = document.getElementById("registerModal");

        if (opcao == 0) {
          registerModal.style.display = 'none';
          loginModal.style.display = 'block';
        } else {
          loginModal.style.display = 'none';
          registerModal.style.display = 'block';
        }
      }
      
      //Início da gambiarra de fechar os modais
      
      var posCima, posEsq, posBaixoLogin, posBaixoRegister, posDir;

      function iniciarVarsFecharModal(){
        posCima = $('#painelLoginModal').offset().top;
        posEsq = $('#painelLoginModal').offset().left;
        posBaixoLogin = posCima + $('#painelLoginModal').outerHeight();
        posDir = posEsq + $('#painelLoginModal').outerWidth();
      }

      function iniciarVars2FecharModal(){
        posBaixoRegister = posCima + $('#painelRegisterModal').outerHeight();
      }

      $('#loginModal').click(function(e){
        if (e.clientX < posEsq || e.clientX > posDir || e.clientY < posCima || e.clientY > posBaixoLogin) {
          loginModal.style.display = 'none';
        }
      });

      $('#registerModal').click(function(e){
        if (e.clientX < posEsq || e.clientX > posDir || e.clientY < posCima || e.clientY > posBaixoRegister) {
          registerModal.style.display = 'none';
        }
      });

      //Final da gambiarra de fechar os modais

// // tamanho da tela
//   function myFunction(x) {
//     if (x.matches) {
//       document.body.style.backgroundColor = "yellow";
//     } else {
//       // document.body.style.backgroundColor = "pink";
//     }
// }
//   var x = window.matchMedia("(max-width: 700px)")
//   myFunction(x)
//   x.addListener(myFunction)



// PAGINAÇÃO CONFRONTOS
var $tableConf = document.getElementById("tableConf"),
$nC = 6,
$rowCountC = $tableConf.rows.length,
$firstRowC = $tableConf.rows[0].firstElementChild.tagName,
$hasHeadC = ($firstRowC === "TH"),
$trC = [],
$iC,$iiC,$jC = ($hasHeadC)?1:0,
$thC = ($hasHeadC?$tableConf.rows[(0)].outerHTML:"");
var $pageCountC = Math.ceil($rowCountC / $nC);
if ($pageCountC > 1) {
  for ($iC = $jC,$iiC = 0; $iC < $rowCountC; $iC++, $iiC++)
    $trC[$iiC] = $tableConf.rows[$iC].outerHTML;
  $tableConf.insertAdjacentHTML("afterend","<div id='buttons' class='d-flex justify-content-center'></div");
  sort(1);
}

function sort($pC) {
  var $rowsC = $thC,$sC = (($nC * $pC)-$nC);
  for ($iC = $sC; $iC < ($sC+$nC) && $iC < $trC.length; $iC++)
    $rowsC += $trC[$iC];
  
  $tableConf.innerHTML = $rowsC;
  document.getElementById("buttons").innerHTML = pageButtons($pageCountC,$pC);
  document.getElementById("id"+$pC).setAttribute("class","active");
}

function pageButtons($pCountC,$curC) {
  var $prevDisC = ($curC == 1)?"disabled":"",
    $nextDisC = ($curC == $pCountC)?"disabled":"",
    $buttonsC = "<ul class='pagination'><li class='page-item'><input type='button' class='pag pag-link' value='&lsaquo;' onclick='sort("+($curC - 1)+")' "+$prevDisC+"></li>";
  for ($iC=1; $iC<=$pCountC;$iC++)
    $buttonsC += "<li class='page-item'><input type='button' class='pag pag-link' id='id"+$iC+"'value='"+$iC+"' onclick='sort("+$iC+")'></li>";
  $buttonsC += "<li class='page-item'><input type='button' class='pag pag-link' value='&rsaquo;' onclick='sort("+($curC + 1)+")' "+$nextDisC+"></li></ul>";
  return $buttonsC;
}
// // PAGINAÇÃO BAR
// var $tableBar = document.getElementById("tableBar"),
// $nB = 4,
// $rowCountB = $tableBar.rows.length,
// $firstRowB = $tableBar.rows[0].firstElementChild.tagName,
// $hasHeadB = ($firstRowB === "TH"),
// $trB = [],
// $iB,$iiB,$jB = ($hasHeadB)?1:0,
// $thB = ($hasHeadB?$tableBar.rows[(0)].outerHTML:"");
// var $pageCountB = Math.ceil($rowCountB / $nB);
// if ($pageCountB > 1) {
//   for ($iB = $jB,$iiB = 0; $iB < $rowCountB; $iB++, $iiB++)
//     $trB[$iiB] = $tableBar.rows[$iB].outerHTML;
//   $tableBar.insertAdjacentHTML("afterend","<div id='buttons' class='d-flex justify-content-center'></div");
//   sort(1);
// }

// function sort($pB) {
//   var $rowsB = $thB,$sB = (($nB * $pB)-$nB);
//   for ($iB = $sB; $iB < ($sB+$nB) && $iB < $trB.length; $iB++)
//     $rowsB += $trB[$iB];
  
//   $tableBar.innerHTML = $rowsB;
//   document.getElementById("buttons").innerHTML = pageButtons($pageCountB,$pB);
//   document.getElementById("id"+$pB).setAttribute("class","active");
// }

// function pageButtons($pCountB,$curB) {
//   var $prevDisB = ($curB == 1)?"disabled":"",
//     $nextDisB = ($curB == $pCountB)?"disabled":"",
//     $buttonsB = "<ul class='pagination'><li class='page-item'><input type='button' class='pag pag-link' value='&lsaquo;' onclick='sort("+($curB - 1)+")' "+$prevDisB+"></li>";
//   for ($iB=1; $iB<=$pCountB;$iB++)
//     $buttonsB += "<li class='page-item'><input type='button' class='pag pag-link' id='id"+$iB+"'value='"+$iB+"' onclick='sort("+$iB+")'></li>";
//   $buttonsB += "<li class='page-item'><input type='button' class='pag pag-link' value='&rsaquo;' onclick='sort("+($curB + 1)+")' "+$nextDisB+"></li></ul>";
//   return $buttonsB;
// }


// PAGINAÇÃO PROGRAMAÇÃO
// var $table = document.getElementById("tableProg"),
// $n = 3,
// $rowCount = $table.rows.length,
// $firstRow = $table.rows[0].firstElementChild.tagName,
// $hasHead = ($firstRow === "TH"),
// $tr = [],
// $i,$ii,$j = ($hasHead)?1:0,
// $th = ($hasHead?$table.rows[(0)].outerHTML:"");
// var $pageCount = Math.ceil($rowCount / $n);
// if ($pageCount > 1) {
//   for ($i = $j,$ii = 0; $i < $rowCount; $i++, $ii++)
//     $tr[$ii] = $table.rows[$i].outerHTML;
//   $table.insertAdjacentHTML("afterend","<div id='buttons' class='d-flex justify-content-center'></div");
//   sort(1);
// }

// function sort($p) {
//   var $rows = $th,$s = (($n * $p)-$n);
//   for ($i = $s; $i < ($s+$n) && $i < $tr.length; $i++)
//     $rows += $tr[$i];
  
//   $table.innerHTML = $rows;
//   document.getElementById("buttons").innerHTML = pageButtons($pageCount,$p);
//   document.getElementById("id"+$p).setAttribute("class","active");
// }

// function pageButtons($pCount,$cur) {
//   var $prevDis = ($cur == 1)?"disabled":"",
//     $nextDis = ($cur == $pCount)?"disabled":"",
//     $buttons = "<ul class='pagination'><li class='page-item'><input type='button' class='pag pag-link' value='&lsaquo;' onclick='sort("+($cur - 1)+")' "+$prevDis+"></li>";
//   for ($i=1; $i<=$pCount;$i++)
//     $buttons += "<li class='page-item'><input type='button' class='pag pag-link' id='id"+$i+"'value='"+$i+"' onclick='sort("+$i+")'></li>";
//   $buttons += "<li class='page-item'><input type='button' class='pag pag-link' value='&rsaquo;' onclick='sort("+($cur + 1)+")' "+$nextDis+"></li></ul>";
//   return $buttons;
// }


// PAGINAÇÃO JOGOS
// var $tableBar = document.getElementById("tableBar"),
// $nB = 4,
// $rowCountB = $tableBar.rows.length,
// $firstRowB = $tableBar.rows[0].firstElementChild.tagName,
// $hasHeadB = ($firstRowB === "TH"),
// $trB = [],
// $iB,$iiB,$jB = ($hasHeadB)?1:0,
// $thB = ($hasHeadB?$tableBar.rows[(0)].outerHTML:"");
// var $pageCountB = Math.ceil($rowCountB / $nB);
// if ($pageCountB > 1) {
//   for ($iB = $jB,$iiB = 0; $iB < $rowCountB; $iB++, $iiB++)
//     $trB[$iiB] = $tableBar.rows[$iB].outerHTML;
//   $tableBar.insertAdjacentHTML("afterend","<div id='buttons' class='d-flex justify-content-center'></div");
//   sort(1);
// }

// function sort($pB) {
//   var $rowsB = $thB,$sB = (($nB * $pB)-$nB);
//   for ($iB = $sB; $iB < ($sB+$nB) && $iB < $trB.length; $iB++)
//     $rowsB += $trB[$iB];
  
//   $tableBar.innerHTML = $rowsB;
//   document.getElementById("buttons").innerHTML = pageButtons($pageCountB,$pB);
//   document.getElementById("id"+$pB).setAttribute("class","active");
// }

// function pageButtons($pCountB,$curB) {
//   var $prevDisB = ($curB == 1)?"disabled":"",
//     $nextDisB = ($curB == $pCountB)?"disabled":"",
//     $buttonsB = "<ul class='pagination'><li class='page-item'><input type='button' class='pag pag-link' value='&lsaquo;' onclick='sort("+($curB - 1)+")' "+$prevDisB+"></li>";
//   for ($iB=1; $iB<=$pCountB;$iB++)
//     $buttonsB += "<li class='page-item'><input type='button' class='pag pag-link' id='id"+$iB+"'value='"+$iB+"' onclick='sort("+$iB+")'></li>";
//   $buttonsB += "<li class='page-item'><input type='button' class='pag pag-link' value='&rsaquo;' onclick='sort("+($curB + 1)+")' "+$nextDisB+"></li></ul>";
//   return $buttonsB;
// }
   
    </script>