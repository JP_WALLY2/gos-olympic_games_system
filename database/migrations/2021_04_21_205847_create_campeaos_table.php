<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampeaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campeaos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('times_id')->unsigned()->nullable();
            $table->foreign('times_id')->references('id')->on('times');

            $table->integer('jogos_id')->unsigned()->nullable();
            $table->foreign('jogos_id')->references('id')->on('jogos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campeaos');
    }
}
