<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateConfrontosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confrontos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status', 50)->default('Não Começou');
            $table->string('fase', 50)->nullable();
            $table->integer('times_id')->unsigned()->nullable();
            $table->foreign('times_id')->references('id')->on('times');
            $table->integer('placar1')->default('0');
            $table->integer('subPlacar1')->nullable();

            $table->integer('times_id2')->unsigned()->nullable();
            $table->foreign('times_id2')->references('id')->on('times');
            $table->integer('placar2')->default('0');
            $table->integer('subPlacar2')->nullable();

            $table->integer('jogos_id')->unsigned()->nullable();
            $table->foreign('jogos_id')->references('id')->on('jogos');
            
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('vencedor')->default(0);
            $table->smallInteger('destaque')->default(0);
            // $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('confrontos');
    }
}
