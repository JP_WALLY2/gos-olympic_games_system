<?php

use Illuminate\Database\Seeder;

class JogosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jogos')->insert([
            'nome'   => 'Futebol',
            'categoria' => 'Masc',
            'pontuacao' => 'Gol'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Futebol',
            'categoria' => 'Fem',
            'pontuacao' => 'Gol'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Vôlei',
            'categoria' => 'Fem',
            'pontuacao' => 'Pontos'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Vôlei',
            'categoria' => 'Masc',
            'pontuacao' => 'Pontos'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Uno',
            'pontuacao' => 'Vitórias'

        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Xadrez',
            'pontuacao' => 'Vitórias'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Rouba Bandeira',
            'pontuacao' => 'Vitórias'
        ]);
        
        DB::table('jogos')->insert([
            'nome'   => 'Dominó',
            'pontuacao' => 'Vitórias'
        ]);

    }
}
