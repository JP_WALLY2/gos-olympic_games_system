<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TimesTableSeeder::class);
        $this->call(JogosTableSeeder::class);
        // factory(App\Time::class, 1)->create();
    }
}
