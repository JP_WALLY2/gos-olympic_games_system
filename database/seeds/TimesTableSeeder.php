<?php

use Illuminate\Database\Seeder;

class TimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('times')->insert([
            'nome' => 'Oliveira II',
            'img' => 'img/Oliveira2.png'
            ]);
        DB::table('times')->insert([
            'nome' => 'Batalhão Mirim',
            'img'  => 'img/batalhaoMirim.png'
        ]);

        DB::table('times')->insert([
            'nome' => 'Arroio do Padre II'
        ]);

        DB::table('times')->insert([
            'nome' => 'Dona Júlia'
        ]);

        DB::table('times')->insert([
            'nome' => 'Aliança',
            'img'  => 'img/JEAlianca.png'
        ]);

    }
}
