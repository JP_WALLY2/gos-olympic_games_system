$(document).ready(function(){

  //mascara do preço do admin
  $('.preco').mask("#.##0,00", {reverse: true});
  $('.hora').mask("00:00"), {reverse: true}

  //temporizador da mensagem de status do admin
  setTimeout(function(){
    $('.msg').remove();
  }, 2500);

//Suavizar o scroll do admin
 $('#mainNav a.nav-link').click(function(link){
        link.preventDefault();
        var target = $(this).attr('href');
        let alvo = $(target);
        let pos = alvo.position().top;
        
        $('html, body').animate({
          scrollTop:pos - 75
        }, 1200);

  });

//script de abrir e fechar modal do admin
 function abrirModal(opcao){

        var loginModal = document.getElementById('loginModal');
        var registerModal = document.getElementById("registerModal");

        if (opcao == 0) {
          registerModal.style.display = 'none';
          loginModal.style.display = 'block';
        } else {
          loginModal.style.display = 'none';
          registerModal.style.display = 'block';
        }
      }

      // seleção de times do admin
      iniciarSelectsTimes();
  
      function iniciarSelectsTimes(){
        $("#time1_id option:eq(0)").attr("selected", true);
        $("#time2_id option:eq(0)").attr("selected", true);
        $("#time3_id option:eq(0)").attr("selected", true);
        $("#time4_id option:eq(0)").attr("selected", true);
        $("#time5_id option:eq(0)").attr("selected", true);
      }
      
      var optionAnteriorTime = [];
      var optionAtualTime = [];
      
      optionAnteriorTime[0] = $("#time1_id option:selected");
      optionAnteriorTime[1] = $("#time2_id option:selected");
      optionAnteriorTime[2] = $("#time3_id option:selected");
      optionAnteriorTime[3] = $("#time4_id option:selected");
      optionAnteriorTime[4] = $("#time5_id option:selected");
      
      $(".selectTime").change(function(){
        
        var idSelect = $(this).attr("id");
        var idArraySelect = parseInt(idSelect.charAt(4))-1;
        
        optionAtualTime[idArraySelect] = $("#"+idSelect+" option:selected");
        
        var idOptionAnterior = optionAnteriorTime[idArraySelect].index();
        var idOptionAtual = optionAtualTime[idArraySelect].index();
        
        if (idSelect != "time1_id") {
          $("#time1_id option:eq("+idOptionAnterior+")").show();
          if (idOptionAtual != 0) {
            $("#time1_id option:eq("+idOptionAtual+")").hide();
          }
        }
        
        if (idSelect != "time2_id") {
          $("#time2_id option:eq("+idOptionAnterior+")").show();
          if (idOptionAtual != 0) {
            $("#time2_id option:eq("+idOptionAtual+")").hide();
          }
        }
        
        if (idSelect != "time3_id") {
          $("#time3_id option:eq("+idOptionAnterior+")").show();
          if (idOptionAtual != 0) {
            $("#time3_id option:eq("+idOptionAtual+")").hide();
          }
        }
        
        if (idSelect != "time4_id") {
          $("#time4_id option:eq("+idOptionAnterior+")").show();
          if (idOptionAtual != 0) {
            $("#time4_id option:eq("+idOptionAtual+")").hide();
          }
        }
        
        if (idSelect != "time5_id") {
          $("#time5_id option:eq("+idOptionAnterior+")").show();
          if (idOptionAtual != 0) {
            $("#time5_id option:eq("+idOptionAtual+")").hide();
          }
        }
        
        optionAnteriorTime[idArraySelect] = optionAtualTime[idArraySelect];
      });


})
